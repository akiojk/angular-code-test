import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { Customer } from "../models";
import * as MockData from "../reducers/mock-data";
import { orderBy } from 'lodash';

@Injectable({
  providedIn: "root",
})
export class CustomersService {
  private mockData: Customer[];
  constructor() {
    this.mockData = MockData.customers.slice();
  }

  getCustomers(query?: string, sort?: {field?:string, direction?:string}): Observable<Customer[]> {
    const results = query ? this.mockData.filter((item) => {
      return item.firstName.toLowerCase().indexOf(query.toLowerCase()) > -1
      || item.lastName.toLowerCase().indexOf(query.toLowerCase()) > -1
      || (item.phoneNumber || '').replace(/[^\d]/g, '').indexOf(query) > -1
    }) : this.mockData;
    if(sort && sort.field){
      return of(orderBy(results, [sort.field], [sort.direction == 'asc' ? 'asc' : 'desc']));
    }

    return of(results);
  }

  addCustomer(customer: Customer): Observable<Customer> {
    this.mockData = [...this.mockData, customer];
    return of(this.mockData.find((item) => item.id == customer.id)!);
  }

  deleteCustomer(id: string): Observable<string> {
    this.mockData = this.mockData.filter((customer) => customer.id != id);
    return of(id);
  }

  updateCustomer(customer: Customer): Observable<Customer> {
    this.mockData = this.mockData.filter((item) => item.id != customer.id).concat(customer);
    return of(this.mockData.find((item) => item.id == customer.id)!);
  }
}
