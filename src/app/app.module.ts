import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { StoreModule } from "@ngrx/store";

import { AppRoutingModule } from "./app-routing.module";
import { ReactiveFormsModule } from "@angular/forms";
import { MatInputModule } from "@angular/material/input";

import * as fromState from "./reducers/customer.reducers";

import { AppComponent } from "./app.component";
import { CustomerComponent } from "./components/customer/customer.component";
import { CustomerListComponent } from "./components/customer-list/customer-list.component";
import { AddCustomerComponent } from "./components/add-customer/add-customer.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatButtonModule } from "@angular/material/button";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatIconModule } from "@angular/material/icon";
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { EffectsModule } from '@ngrx/effects'
import { CustomerEffects } from "./effects/customer-effects";
import { MatSortModule } from "@angular/material/sort";
@NgModule({
  declarations: [
    AppComponent,
    CustomerComponent,
    CustomerListComponent,
    AddCustomerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    StoreModule.forRoot({}),
    StoreModule.forFeature(fromState.customerFeatureKey, fromState.reducer),
    BrowserAnimationsModule,
    MatExpansionModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatSortModule,
    BsDropdownModule.forRoot(),
    EffectsModule.forRoot([CustomerEffects])
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
