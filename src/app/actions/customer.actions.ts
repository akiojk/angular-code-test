import { createAction, props } from '@ngrx/store';
import { Customer } from '../models/customer';

export const addCustomer = createAction(
  '[Customer] addCustomer',
  props<{ customer: Customer }>()
);

export const addCustomerSuccess = createAction(
  '[Customer] addCustomerSuccess',
  props<{ customer: Customer }>()
);

export const deleteCustomer = createAction(
  '[Customer] deleteCustomer',
  props<{ id: string }>()
);

export const deleteCustomerSuccess = createAction(
  '[Customer] deleteCustomerSuccess',
  props<{ id: string }>()
);

export const getCustomers = createAction(
  '[Customer] getCustomers',
  props<{ term?: string, sort?: {field?: string, direction?: string} }> ()
);

export const getCustomersSuccess = createAction(
  '[Customer] getCustomersSuccess',
  props<{ customers: Customer[] }> ()
);

export const updateCustomer = createAction(
  '[Customer] updateCustomer',
  props<{ customer: Customer }>()
);

export const updateCustomerSuccess = createAction(
  '[Customer] updateCustomerSuccess',
  props<{ customer: Customer }>()
);