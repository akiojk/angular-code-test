import { Action, createReducer, on } from '@ngrx/store';
import * as CustomerActions from '../actions/customer.actions';
import { Customer } from '../models';

export const customerFeatureKey = 'customer';

export interface State {
  customers: Customer[];
}

export const initialState: State = {
  customers: []
};

const customerReducer = createReducer(
  initialState,

  on(CustomerActions.getCustomersSuccess, (state, { customers }) => {
    return {
      ...state,
      customers: customers
    }
  }),

  on(CustomerActions.addCustomerSuccess, (state, { customer }) => {
    return {
      ...state,
      customers: [...state.customers, { ...customer }],
    };
  }),

  on(CustomerActions.deleteCustomerSuccess, (state, { id }) => {
    return {
      ...state,
      customers: state.customers.filter((customer) => customer.id !== id),
    };
  }),

  on(CustomerActions.updateCustomerSuccess, (state, { customer }) => {
    return {
      ...state,
      customers: state.customers.map(oldCustomer => oldCustomer.id == customer.id ? customer : oldCustomer)
    };
  }),
);

export function reducer(state: State | undefined, action: Action) {
  return customerReducer(state, action);
}
