import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { FormBuilder } from '@angular/forms';

import { AddCustomerComponent } from './add-customer.component';

describe('AddCustomerComponent', () => {
  let component: AddCustomerComponent;
  let fixture: ComponentFixture<AddCustomerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AddCustomerComponent],
      providers: [provideMockStore({}), FormBuilder],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should fail identical names check', () => {
    component.firstNameInput.setValue('cd');
    component.lastNameInput.setValue('cd');
    expect(component.customerFormGroup.valid).toBeFalsy();
  });

  it('should fail under 18 check', () => {
    component.firstNameInput.setValue('cd');
    component.lastNameInput.setValue('ab');
    component.dobInput.setValue(new Date(2099,1,1));
    expect(component.customerFormGroup.valid).toBeFalsy();
  });
});
