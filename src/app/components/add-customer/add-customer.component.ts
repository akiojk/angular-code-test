import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from "@angular/core";
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from "@angular/forms";
import { Store } from "@ngrx/store";
import { addCustomer, updateCustomer } from "../../actions/customer.actions";
import { Customer } from "../../models";
import * as CustomerStore from "../../reducers/customer.reducers";
import * as uuid from 'uuid';

@Component({
  selector: "app-add-customer",
  templateUrl: "./add-customer.component.html",
  styleUrls: ["./add-customer.component.scss"],
})
export class AddCustomerComponent implements OnChanges {

  @Output() onClose:EventEmitter<any> = new EventEmitter();
  @Input() customer?:Customer;

  customerFormGroup: FormGroup; 
  nameInputsGroup: FormGroup;
  dobInput: FormControl = new FormControl(null, [this.over18Validator]);
  firstNameInput: FormControl = new FormControl('', [Validators.required]);
  lastNameInput: FormControl = new FormControl('', [Validators.required]);
  phoneInput: FormControl = new FormControl('', [Validators.pattern(/^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2-57-8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/gm)]);

  constructor(
    private store: Store<CustomerStore.State>,
    private formBuilder: FormBuilder
  ) {
    this.nameInputsGroup = this.formBuilder.group({
      firstName: this.firstNameInput,
      lastName: this.lastNameInput
    }, {validators: [this.identicalNamesValidator]});

    this.customerFormGroup = this.formBuilder.group({
      names: this.nameInputsGroup,
      phoneNumber: this.phoneInput,
      dob: this.dobInput
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes['customer'] && this.customer){
      this.updateFormValues();      
    }
  }

  updateFormValues(){
    if(this.customer){
      this.firstNameInput.setValue(this.customer.firstName);
      this.lastNameInput.setValue(this.customer.lastName);
      this.phoneInput.setValue(this.customer.phoneNumber);
      this.dobInput.setValue(this.customer.dob);
    }
  }

  onSubmit(): void {
    if(this.customer){ 
      this.store.dispatch(updateCustomer({customer: {
        firstName: this.firstNameInput.value, 
        lastName: this.lastNameInput.value,
        phoneNumber: this.phoneInput.value,
        dob: this.dobInput.value,
        id: this.customer.id}}));
    }else{
      this.store.dispatch(
        addCustomer({
          customer: { 
            firstName: this.firstNameInput.value, 
            lastName: this.lastNameInput.value, 
            phoneNumber: this.phoneInput.value, 
            dob: this.dobInput.value ? new Date(this.dobInput.value) : null,
            id: uuid.v4()
          } as Customer,
        })
      );
    }
    this.cleanup();
  }

  cleanup(): void {
    this.customerFormGroup.reset();
    this.customerFormGroup.markAsUntouched();
    this.onClose.emit();
  }

  over18Validator(control:AbstractControl):ValidationErrors | null{
    if(!control.value){
      return null;
    }
    return (Date.now() - new Date(control.value).getTime()) / 1000 >= 18 * 365 * 24 * 60 * 60 ? null : { under18: true };
  }

  identicalNamesValidator(control:AbstractControl){
    let firstName:string = control.get('firstName')?.value;
    let lastName:string = control.get('lastName')?.value;
    if(firstName && lastName && firstName.toLowerCase() == lastName.toLowerCase()){
      return { identical: true };
    }
    return null;
  }
}
