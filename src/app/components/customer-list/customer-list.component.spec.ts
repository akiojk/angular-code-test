import { ComponentFixture, inject, TestBed } from "@angular/core/testing";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { EffectsModule } from "@ngrx/effects";
import { Store, StoreModule } from "@ngrx/store";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { of } from "rxjs";
import { CustomerEffects } from "src/app/effects/customer-effects";
import { selectCustomers } from "src/app/selectors/customer.selectors";
import { CustomersService } from "src/app/services/customers.service";
import * as fromState from "../../reducers/customer.reducers";
import * as MockData from "../../reducers/mock-data";
import { CustomerListComponent } from "./customer-list.component";

describe("CustomerListComponent", () => {
  let component: CustomerListComponent;
  let fixture: ComponentFixture<CustomerListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        BsDropdownModule.forRoot(),
        BrowserAnimationsModule,
        EffectsModule.forRoot([CustomerEffects]),
        StoreModule.forRoot({}),
        StoreModule.forFeature(fromState.customerFeatureKey, fromState.reducer)
      ],
      declarations: [CustomerListComponent],
      providers: [
        {
          provide: CustomersService,
          useClass: StubCustomersService,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should show correct number of customers", function (done) {
    inject([Store], (store: Store) => {
      store.select(selectCustomers).subscribe(
        () => {
          expect(component.customersDataSource.data.length).toEqual(MockData.customers.length);
          done();
        },
        () => done()
      );
    })();
  });
});

class StubCustomersService {
  getCustomers() {
    return of(MockData.customers);
  }
}
