import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { Store } from "@ngrx/store";
import { Subscription } from "rxjs";
import { deleteCustomer, getCustomers } from "../../actions/customer.actions";
import { Customer } from "../../models";
import * as CustomerStore from "../../reducers/customer.reducers";
import { selectCustomers } from "../../selectors/customer.selectors";

@Component({
  selector: "app-customer-list",
  templateUrl: "./customer-list.component.html",
  styleUrls: ["./customer-list.component.scss"],
})
export class CustomerListComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;
  customersDataSource: MatTableDataSource<Customer> = new MatTableDataSource<Customer>(
    []
  ); 

  displayedColumns: string[] = [
    "firstName",
    "lastName",
    "phoneNumber",
    "dob",
    "actions",
  ];
  private customerSubscription: Subscription;
  private searchTerm: string = '';
  private sort: {field?: string, direction?: string} = {};

  constructor(private store: Store<CustomerStore.State>) {
    this.customerSubscription = this.store
      .select(selectCustomers)
      .subscribe((customers) => {
        this.customersDataSource = new MatTableDataSource<Customer>(customers);
      });
  }
  
  ngOnInit(): void {
    this.store.dispatch(getCustomers({}));
  }  

  ngOnDestroy(): void {
    this.customerSubscription.unsubscribe();
  }

  ngAfterViewInit(): void {
    // this.customers.paginator = this.paginator!;
  }
 
  applySearch(term: string){
    this.searchTerm = term;
    this.updateDatasource();
  }

  deleteCustomer(customer: Customer): void {
    this.store.dispatch(deleteCustomer({ id: customer.id }));
  }

  sortDataSource(event:any){
    if(!event.direction){
      this.sort = {};
    }else{
      this.sort = {field: event.active, direction: event.direction};
    }
    this.updateDatasource();
  }

  private updateDatasource(){
    this.store.dispatch(getCustomers({term: this.searchTerm, sort: this.sort}));
  }
}
