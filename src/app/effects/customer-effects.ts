import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { empty } from "rxjs";
import { catchError, map, switchMap } from "rxjs/operators";
import { addCustomer, addCustomerSuccess, deleteCustomer, deleteCustomerSuccess, getCustomers, getCustomersSuccess, updateCustomer, updateCustomerSuccess } from "../actions/customer.actions";
import { CustomersService } from "../services/customers.service";

@Injectable()
export class CustomerEffects {
    constructor(
        private actions$:Actions,
        private customerApiService: CustomersService
    ){}
    
    getCustomers$ = createEffect(() =>  
        this.actions$.pipe(
            ofType(getCustomers),
            switchMap(action => this.customerApiService.getCustomers(action.term, action.sort).pipe(
                map(response => getCustomersSuccess({customers: response})),
                catchError((err) => { console.log(err); return empty(); })
            ))
        )
    );

    addCustomer$ = createEffect(() => 
        this.actions$.pipe(
            ofType(addCustomer),
            switchMap(action => this.customerApiService.addCustomer(action.customer).pipe(
                map(response => addCustomerSuccess({customer: response})),
                catchError((err) => { console.log(err); return empty(); })
            ))
        )
    );

    deleteCustomer$ = createEffect(() => 
        this.actions$.pipe(
            ofType(deleteCustomer),
            switchMap(action => this.customerApiService.deleteCustomer(action.id).pipe(
                map(response => deleteCustomerSuccess({id: response})),
                catchError((err) => { console.log(err); return empty(); })
            ))
        )
    );

    updateCustomer$ = createEffect(() =>
        this.actions$.pipe(
            ofType(updateCustomer),
            switchMap(action => this.customerApiService.updateCustomer(action.customer).pipe(
                map(response => updateCustomerSuccess({customer: response})),
                catchError((err) => { console.log(err); return empty(); })
            ))
        )
    );
}
